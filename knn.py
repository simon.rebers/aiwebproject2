def distance_function(user, current_user):
    """Return the id of a user and its distance to the current user"""
    dist = 0
    user_id, user_dict = user
    for movie_id in current_user.keys():
        if user_dict.get(movie_id):
            dist += abs(user_dict.get(movie_id) - current_user.get(movie_id))
        else:
            dist += 1.6  # penalty for not having watched same movie
            # 1.6 is the average distance between two ratings
            # if penalty is higher knn finds users mostly because they share a high amount of movies
            # if penalty is lower knn finds users mostly because they share a low amount of movies

    return user_id, round(dist, 3)


def knn(users, current_user, k):

    # get a list with the user, it's different to the current user and how many movies they have in common
    dist_list = [(distance_function(user, current_user)) for user in users]

    # sort this list by the distance
    dist_list.sort(key=lambda user: user[1])

    # return the k nearest neighbours
    print(dist_list[:k])
    return dist_list[:k]
