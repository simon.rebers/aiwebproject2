import csv
from datetime import datetime

import random
import tqdm
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.exc import IntegrityError

from models import Movie, MovieGenre, Link, Tag, Rating, User

# limit of ratings imported for testing
LIMIT = 10000000


def check_and_read_data(db: SQLAlchemy):
    # check if we have ratings in the database
    # read data if database is empty

    if Rating.query.count() == 0:
        for index, row in enumerate(__csv_to_db('data/ratings.csv', db)):
            if index == LIMIT:
                break
            link = User(
                id=row["userId"],
                username=str(random.randint(0, 10000)),
                password=str(random.randint(0, 10000)),
            )
            db.session.add(link)

        for index, row in enumerate(__csv_to_db('data/ratings.csv', db)):
            if index == LIMIT:
                break
            link = Rating(
                id=index,
                user_id=row["userId"],
                movie_id=row["movieId"],
                rating=row["rating"],
                timestamp=row["timestamp"],
            )
            db.session.add(link)

    # check if we have movies in the database
    # read data if database is empty
    if Movie.query.count() == 0:
        # read movies from csv
        for row in __csv_to_db('data/movies.csv', db):
            link = Movie(
                id=row["movieId"],
                title=row["title"],
            )
            db.session.add(link)

            for genre in row["genres"].split('|'):  # add each genre to the movie_genre table
                db.session.add(MovieGenre(
                    movie_id=link.id,
                    genre=genre,
                ))

    # check if we have links in the database
    # read data if database is empty
    if Link.query.count() == 0:
        for row in __csv_to_db('data/links.csv', db):
            link = Link(
                movie_id=row["movieId"],
                imdb_id=row["imdbId"],
                tmdb_id=row["tmdbId"],
            )
            db.session.add(link)

    # check if we have tags in the database
    # read data if database is empty
    if Tag.query.count() == 0:
        for row in __csv_to_db('data/tags.csv', db):
            link = Tag(
                user_id=row["userId"],
                movie_id=row["movieId"],
                tag=row["tag"],
                timestamp=datetime.fromtimestamp(int(row["timestamp"])),
            )
            db.session.add(link)


def __csv_to_db(fn: str, db: SQLAlchemy):
    print(f"Reading {fn}...")

    with open(fn, newline='', encoding='utf8') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        for count, row in tqdm.tqdm(enumerate(reader)):
            try:
                yield row
                db.session.commit()  # save data to database
            except IntegrityError:
                print(f"Ignore duplicate (row #{count+1}): {str(row)[:90] + '...' if len(str(row)) > 90 else str(row)}")
                db.session.rollback()
                pass

    print(f"Read {count + 1} items from {fn}.")