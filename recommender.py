# Contains parts from: https://flask-user.readthedocs.io/en/latest/quickstart_app.html
import time

from flask import Flask, render_template, request
from flask_user import login_required, UserManager, current_user
from sqlalchemy.sql.functions import user

from models import db, User, Movie, MovieGenre, Tag, Link, Rating
from read_data import check_and_read_data
from knn import knn


# Class-based application configuration
class ConfigClass(object):
    """ Flask application config """

    # Flask settings
    SECRET_KEY = 'This is an INSECURE secret!! DO NOT use this in production!!'

    # Flask-SQLAlchemy settings
    SQLALCHEMY_DATABASE_URI = 'sqlite:///movie_recommender.sqlite'  # File-based SQL database
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # Avoids SQLAlchemy warning

    # Flask-User settings
    USER_APP_NAME = "Movie Recommender"  # Shown in and email templates and page footers
    USER_ENABLE_EMAIL = False  # Disable email authentication
    USER_ENABLE_USERNAME = True  # Enable username authentication
    USER_REQUIRE_RETYPE_PASSWORD = True  # Simplify register form

    # make sure we redirect to home view, not /
    # (otherwise paths for registering, login and logout will not work on the server)
    USER_AFTER_LOGIN_ENDPOINT = 'home_page'
    USER_AFTER_LOGOUT_ENDPOINT = 'home_page'
    USER_AFTER_REGISTER_ENDPOINT = 'home_page'

# Create Flask app
app = Flask(__name__)
app.config.from_object(__name__ + '.ConfigClass')  # configuration
app.app_context().push()  # create an app context before initializing db
db.init_app(app)  # initialize database
db.create_all()  # create database if necessary
user_manager = UserManager(app, db, User)  # initialize Flask-User management


@app.cli.command('initdb')
def initdb_command():
    global db
    """Creates the database tables."""
    check_and_read_data(db)
    print('Initialized the database.')


# The Home page is accessible to anyone
@app.route('/')
def home_page():
    # render home.html template
    return render_template("home.html")


# The Members page is only accessible to authenticated users via the @login_required decorator
@app.route('/movies')
@login_required  # User must be authenticated
def movies_page():
    search_word = request.args.get("search_word")

    if search_word:
        movies = db.session.query(Movie).filter(Movie.title.ilike(f"%{search_word}%")).limit(10).all()
    else:
        movies = db.session.query(Movie).limit(10).all()
        search_word = ""

    movie_list = []

    for m in movies:
        tags = Tag.query.filter_by(movie_id=m.id).all()
        link_data = db.session.get(Link, m.id)
        links = (("https://www.imdb.com/title/tt" + str(link_data.imdb_id)), "https://www.themoviedb.org/movie/" + str(link_data.tmdb_id))
        movie_list.append((m, tags, links))

    return render_template("movies.html", movies=movie_list, search_word=search_word)


@app.route('/rate', methods=['POST'])
@login_required  # User must be authenticated
def rate():
    movie_id = request.form.get('movieid')
    rating = request.form.get('rating')
    user_id = current_user.id
    timestamp = round(time.time())
    
    # search in the data if rating already exists
    rated_movie = Rating.query \
        .filter(Rating.user_id == user_id) \
        .filter(Rating.movie_id == movie_id) \
        .all()
    
    if not rated_movie:
        new_rating = Rating(
            id=Rating.query.count(),
            user_id=user_id,
            movie_id=movie_id,
            rating=rating,
            timestamp=timestamp,
        )
        db.session.add(new_rating)
    else:
        # if rating exists, just update rating
        rated_movie[0].rating = rating

    db.session.commit()

    print("Rated {} for {} by {}".format(rating, movie_id, user_id))

    return render_template("rated.html", rating=rating)


@app.route('/recommendations')
@login_required  # User must be authenticated
def recommendation_page():
    return render_template("recommendations.html")


@app.route('/load_recommendations', methods=['POST'])
@login_required
def load_recommendations():
    movie_recommendations = get_recommendations()
    # movie_recommendations tuple order: movie_id, title, genres, imdb_link, tmdb_link
    
    return render_template("load_recommendations.html", movies=movie_recommendations)


@app.route('/rated_movies')
@login_required  # User must be authenticated
def rated_movies_page():
    user_id = current_user.id
    ratings = Rating.query.filter_by(user_id=user_id).all()

    rated_movies = []

    for r in ratings:
        rating = r.rating
        movie_id = r.movie_id
        movie = db.session.get(Movie, movie_id)
        link = db.session.get(Link, movie_id)
        title = movie.title
        genres = movie.genres
        imdb_link = "https://www.imdb.com/title/tt" + str(link.imdb_id)
        tmdb_link = "https://www.themoviedb.org/movie/" + str(link.tmdb_id)
        rated_movies.append((movie_id, title, genres, imdb_link, tmdb_link, rating))
        
    return render_template("rated_movies.html", movies=rated_movies)


def get_recommendations():

    user_id = current_user.id
    user_object = db.session.get(User, user_id)

    # create a dictionary with the ratings of the current user
    rated_movies = {}
    for r in user_object.ratings:
        rated_movies[r.movie_id] = r.rating

    if not user_object.ratings:
        return None

    user_ratings = Rating.query.filter(Rating.movie_id.in_(rated_movies.keys())).filter(User.id != user_id).distinct()

    user_dict = {}

    # create the user dictionary from the user_ratings
    for ur in user_ratings:
        if ur.user_id not in user_dict:
            user_dict[ur.user_id] = {}
        user_dict[ur.user_id][ur.movie_id] = ur.rating

    # calculate a fitting amount of neighbours
    neighbour_amount = max(10, 30 - 5 * len(rated_movies))

    neighbours = knn(user_dict.items(), rated_movies, neighbour_amount)

    dict_neighbours = dict(neighbours)

    # find good rated movies of the neighbours
    five_star_ratings = Rating.query \
        .filter(Rating.user_id.in_([user_id for user_id, _ in neighbours])) \
        .filter(Rating.rating == 5) \
        .all()

    five_star_movies = set([x.movie_id for x in five_star_ratings])

    # filter out movies the current user already rated
    five_star_movies = filter(lambda x: x not in rated_movies.keys(), five_star_movies)

    # find the ratings of all neighbours for the five-star movies
    fs_movies_ratings = Rating.query \
        .filter(Rating.user_id.in_([user_id for user_id, _ in neighbours])) \
        .filter(Rating.movie_id.in_(five_star_movies)) \
        .all()

    movie_scores = {}
    neighbours_dist_total = round(sum([dist for _, dist in neighbours]), 3)

    # calculate rating scores with the ratings
    for r in fs_movies_ratings:
        current_movie = r.movie_id

        if neighbours_dist_total == 0:  # if all neighbours have a knn-distance of zero no weighting is needed
            rating_score = r.rating
        else:  # ratings are weighted by the users knn-distance
            # for low ratings the score is reduced for higher ratings it is increased, all respective to the weighting
            rating_score = round(((neighbours_dist_total - dict_neighbours.get(r.user_id)) / neighbours_dist_total) * (r.rating - 3), 5)

        if movie_scores.get(current_movie):
            movie_scores[current_movie] += rating_score
        else:
            movie_scores[current_movie] = rating_score

    # sort by scores
    movie_scores = sorted(movie_scores.items(), key=lambda x: x[1], reverse=True)

    movie_recommendations = []

    limit = 10  # number of recommendations shown

    for index, m in enumerate(movie_scores):
        if index == limit:
            break
        movie = db.session.get(Movie, m[0])
        link = db.session.get(Link, m[0])
        title = movie.title
        genres = movie.genres
        imdb_link = "https://www.imdb.com/title/tt" + str(link.imdb_id)
        tmdb_link = "https://www.themoviedb.org/movie/" + str(link.tmdb_id)
        movie_recommendations.append((m[0], title, genres, imdb_link, tmdb_link))

    return movie_recommendations


@app.route('/delete', methods=['POST'])
@login_required
def delete():
    user_id = current_user.id
    db.session.query(Rating).filter(Rating.user_id == user_id).delete()
    db.session.commit()
    print("ratings deleted")
    return render_template("deleted.html")


@app.route('/load', methods=['POST'])
@login_required
def load():
    movie_numbers = int(request.form.get('movieNumbers'))
    search_word = request.form.get('searchWord')

    movies = db.session.query(Movie).filter(Movie.title.ilike(f"%{search_word}%")).slice(movie_numbers, movie_numbers + 10)

    # xe the movie data for the template
    movie_list = []

    for m in movies:
        tags = Tag.query.filter_by(movie_id=m.id).all()
        link_data = db.session.get(Link, m.id)
        links = (("https://www.imdb.com/title/tt" + str(link_data.imdb_id)), "https://www.themoviedb.org/movie/" + str(link_data.tmdb_id))
        movie_list.append((m, tags, links))

    return render_template("load_more.html", movies=movie_list)


# Start development web server
if __name__ == '__main__':
    app.run(port=5000, debug=True)
